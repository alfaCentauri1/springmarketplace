package com.alfaCentauri.cron;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaCronApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaCronApplication.class, args);
	}

}
